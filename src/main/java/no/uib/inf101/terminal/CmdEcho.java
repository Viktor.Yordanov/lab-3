package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] args) {
        String returnString = "";
        for (String i : args) {
            returnString +=     (i + " ");
        }
        return returnString;
    }

    @Override
    public String getName() {
        return "echo";
    }
    
}
